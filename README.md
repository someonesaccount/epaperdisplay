* To Run:
* xvfb-run python myDisplay.py <darksky url>
* To Run In Sim Mode:
* python myDisplay.py <darksky url> SIM
* License: GPL3