#!/usr/local/bin/python
# -*- coding: utf-8 -*-
##
 #  @filename   :   main.cpp
 #  @brief      :   7.5inch e-paper display demo
 #  @author     :   Yehui from Waveshare
 #
 #  Copyright (C) Waveshare     July 28 2017
 #
 # Permission is hereby granted, free of charge, to any person obtaining a copy
 # of this software and associated documnetation files (the "Software"), to deal
 # in the Software without restriction, including without limitation the rights
 # to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 # copies of the Software, and to permit persons to  whom the Software is
 # furished to do so, subject to the following conditions:
 #
 # The above copyright notice and this permission notice shall be included in
 # all copies or substantial portions of the Software.
 #
 # THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 # IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 # FITNESS OR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 # AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 # LIABILITY WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 # OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 # THE SOFTWARE.
 ##

#import epd7in5
from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont
import PIL.ImageOps

import calendar
import time
import requests
import sys  
import urllib, json
import urllib2
import operator
import os 
import random
import threading
import datetime
import re
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
from mainwindow import Ui_MainWindow
import json
from datetime import datetime
import requests
import argparse
import textwrap
from dateutil import tz

reload(sys)  
sys.setdefaultencoding('utf-8')

EPD_WIDTH = 640
EPD_HEIGHT = 384

class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self._dirty = True
        self.setupUi(self)
        self.show()
        self._monthArray = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December']
        self._weekdayArray = [
            'Mon',
            'Tue',
            'Wed',
            'Thu',
            'Fri',
            'Sat',
            'Sun']
        self._fullWeekdayArray = [
            'Monday',
            'Tuesday',
            'Wednesday',
            'Thursday',
            'Friday',
            'Saturday',
            'Sunday']
        self._icons={
            u'clear-day':u'☀',
            u'clear-night':u'☀',
            u'partly-cloudy-day':u'⛅',
            u'partly-cloudy-night':u'⛅',
            u'cloudy':u'☁',
            u'rain':u'☔',
            u'sleet':u'☃',
            u'snow':u'☃',
            u'fog':u'♒',
            u'wind':u'⚟'
            }
        
    def _setText(self, obj, text):
        if obj.text() != text:
            print(str(obj) + ": " + obj.text() + "!=" + text)
            self._dirty = True
            obj.setText(text)
            
    def updateFromJson(self, json):
        self._setCurDayTitle(json)
        self._setCurForcastSummary(json)
        self._setCurWeather(json)
        self._setHourlyWeather(json)
        self._setDailyWeather(json)
    
    def _setCurDayTitle(self, json):
        curDate = self._convertTime(datetime.utcfromtimestamp(int(json['currently']['time'])))
        print(curDate)
        curDayTitleText = self._fullWeekdayArray[curDate.weekday()] + " "
        curDayTitleText += self._monthArray[curDate.month-1] + " "
        curDayTitleText += str(curDate.day) + " "
        curDayTitleText += str(curDate.year)
        
        self._setText(self.currentDayTitle, curDayTitleText)
        
    def _setCurForcastSummary(self, json):
        wrapLength = 28
        text = "\n".join(textwrap.wrap("Currently: " + json['currently']['summary'], wrapLength)) + "\n"
        text += "\n".join(textwrap.wrap("Today: " + json['daily']['data'][0]['summary'], wrapLength))
        self._setText(self.curForcast, text)

    def _setCurWeather(self, json):
        self._setText(self.curWindLabel, "Wind: " + str(int(json['currently']['windSpeed'])) + "mph")
        
        rainPercent = int(float(json['currently']['precipProbability']) * 100.0)
        self._setText(self.curRainLabel, "Rain: " + str(rainPercent) + "%")
        self._setText(self.curTempLabel, "Temp: " + str(int(json['currently']['temperature'])) + "°f")
        self._setText(self.curRTempLabel,"RT: " +
                                   str(int(json['currently']['apparentTemperature'])) + "°f")
        humidity = int(float(json['currently']['humidity']) * 100.0)
        self._setText(self.curHumLabel, "Hum: " + str(humidity) + "%")
        self._setText(self.icon, self._icons[json['currently']['icon']])
        
    def _setHourlyWeather(self, json):
        labelArray = [
            {'rain': self.hour1RainLabel, 'temp': self.hour1TempLabel, 'time': self.hour1TimeLabel},
            {'rain': self.hour2RainLabel, 'temp': self.hour2TempLabel, 'time': self.hour2TimeLabel},
            {'rain': self.hour3RainLabel, 'temp': self.hour3TempLabel, 'time': self.hour3TimeLabel},
            {'rain': self.hour4RainLabel, 'temp': self.hour4TempLabel, 'time': self.hour4TimeLabel},
            {'rain': self.hour5RainLabel, 'temp': self.hour5TempLabel, 'time': self.hour5TimeLabel},
            {'rain': self.hour6RainLabel, 'temp': self.hour6TempLabel, 'time': self.hour6TimeLabel},
            {'rain': self.hour7RainLabel, 'temp': self.hour7TempLabel, 'time': self.hour7TimeLabel},
            {'rain': self.hour8RainLabel, 'temp': self.hour8TempLabel, 'time': self.hour8TimeLabel}]
        for i in range(0, 8):
            data = json['hourly']['data'][i+1]
            theDate = self._convertTime(datetime.utcfromtimestamp(int(data['time'])))
            postfix = "AM"
            theTime = str(theDate.hour)
            if theDate.hour > 12:
                postfix = "PM"
                theTime = str(theDate.hour-12)
            elif theDate.hour == 0:
                theTime = "12"
            theTime += postfix
            
            self._setText(labelArray[i]['time'], theTime)
            self._setText(labelArray[i]['rain'], str(int(float(data['precipProbability'])*100.0)))
            self._setText(labelArray[i]['temp'], str(int(data['apparentTemperature'])))
            
    def _setDailyWeather(self, json):
        labelArray = [
            {'rain': self.day1RainLabel, 'temp': self.day1TempLabel, 'day': self.day1DayLabel, 'dow': self.day1DOWLabel},
            {'rain': self.day2RainLabel, 'temp': self.day2TempLabel, 'day': self.day2DayLabel, 'dow': self.day2DOWLabel},
            {'rain': self.day3RainLabel, 'temp': self.day3TempLabel, 'day': self.day3DayLabel, 'dow': self.day3DOWLabel},
            {'rain': self.day4RainLabel, 'temp': self.day4TempLabel, 'day': self.day4DayLabel, 'dow': self.day4DOWLabel},
            {'rain': self.day5RainLabel, 'temp': self.day5TempLabel, 'day': self.day5DayLabel, 'dow': self.day5DOWLabel},
            {'rain': self.day6RainLabel, 'temp': self.day6TempLabel, 'day': self.day6DayLabel, 'dow': self.day6DOWLabel},
            {'rain': self.day7RainLabel, 'temp': self.day7TempLabel, 'day': self.day7DayLabel, 'dow': self.day7DOWLabel},
            {'rain': self.day8RainLabel, 'temp': self.day8TempLabel, 'day': self.day8DayLabel, 'dow': self.day8DOWLabel}]
        for i in range(0, 8):
            data = json['daily']['data'][i]
            theDate = self._convertTime(datetime.utcfromtimestamp(int(data['time'])))
            
            self._setText(labelArray[i]['dow'], self._weekdayArray[theDate.weekday()])
            self._setText(labelArray[i]['day'], str(theDate.day))
            self._setText(labelArray[i]['rain'], str(int(float(data['precipProbability'])*100.0)))
            self._setText(labelArray[i]['temp'], str(int(data['apparentTemperatureMax'])))
            
    def _convertTime(self, utc):
        from_zone = tz.gettz('UTC')
        to_zone = tz.tzlocal()
        utc = utc.replace(tzinfo=from_zone)
        return utc.astimezone(to_zone)

    def render(self, painter):
        print("My Render Called")
        self._dirty = False
        super(QMainWindow, self).render(painter)

    def isDirty(self):
        return self._dirty
    
if __name__ == '__main__':    
    if len(sys.argv) > 2:
        app = QApplication([])
        app.setApplicationName("Weather Display")

        window = MainWindow()
        img = QImage(window.size(), QImage.Format_Grayscale8)
        painter = QPainter(img)
        window.render(painter)

        req = requests.get(sys.argv[1])
        window.updateFromJson(req.json())
        app.exec_()
        
    else:
        import epd7in5
        import Image
        import ImageDraw
        import ImageFont
        import cStringIO
        
        EPD_WIDTH = 640
        EPD_HEIGHT = 384

        epd = epd7in5.EPD()
        epd.init()

        image = Image.new('1', (EPD_WIDTH, EPD_HEIGHT), 1)    # 1: clear the frame
        draw = ImageDraw.Draw(image)

        app = QApplication([])
        app.setApplicationName("Weather Display")
        
        window = MainWindow()
        img = QImage(window.size(), QImage.Format_Grayscale8)
        painter = QPainter(img)
        invert = True
        while True:
            req = requests.get(sys.argv[1])
            window.updateFromJson(req.json())

            if window.isDirty():
                print("Is Dirty")
                window.render(painter)

                buffer = QBuffer()
                buffer.open(QIODevice.ReadWrite)
                img.save(buffer, "PNG")
        
                strio = cStringIO.StringIO()
                strio.write(buffer.data())
                buffer.close()
                strio.seek(0)
                image = Image.open(strio)
                if invert:
                    epd.display_frame(epd.get_frame_buffer(PIL.ImageOps.invert(image.rotate(180))))
                else:
                    epd.display_frame(epd.get_frame_buffer(image.rotate(180)))
            else:
                print("Not Dirty")
            time.sleep(300)
